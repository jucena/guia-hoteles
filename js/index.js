$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({ interval: 3000 });
    
    $('#contacto').on('show.bs.modal', function(e){
        $('.contactoBtn').each(function(i,el){
            el.classList.remove('btn-outline-success');
            el.classList.add('btn-primary');
            el.setAttribute('disabled', true);
        });
    });
    $('#contacto').on('hidden.bs.modal', function(e){
        $('.contactoBtn').each(function(i,el){
            el.classList.remove('btn-primary');
            el.classList.add('btn-outline-success');
            el.removeAttribute('disabled');
        });
    });

    // llena campos en el modal
    $('#recipiente-name').val('carlos@email.com');
    $('#mensaje-text').val('Por favor me ofrecen costos de 3 dias 4 noches');
});